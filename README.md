# cicd-stage-verify-dast-nuclei

CI/CD Pipeline stage verfiy for an automted dynamic application security test (DAST) with the nuclei project (https://github.com/projectdiscovery/nuclei)

## Getting started

- clone the gitlab repository
- edit the target.txt file
- commit to branch `main`

### Add CICD Jobs from Stage DAST
```
include: 
    - remote: 'https://gitlaburl.com/repository/.cicd-stage-dast-jobs.yml'

variables:
   GIT_SSL_NO_VERIFY: "1"
   NUCLEI_TARGET_FILE: "target.txt"
   NUCLEI_TAG: "cve"
   NUCLEI_REPORT_MARKDOWN: "public/"
   NUCLEI_REPORT_SARIF: "sarif.json"
   NUCLEI_LOG_RESPONSE: "nuclei_resonse.log"


sec_scan_nuclei:      
  stage: verify
  extends: .dast_nuclei


```

### Add CICD Jobs from Stage DAST
```
# .gitlab-ci.yml
include: 
    - remote: 'https://gitlaburl.com/repository/.cicd-stage-verify-dast-nuclei.yml'

variables:
   GIT_SSL_NO_VERIFY: "1"
   NUCLEI_TARGET_FILE: "target.txt"
   NUCLEI_TAG: "cve"
   NUCLEI_REPORT_MARKDOWN: "public/"
   NUCLEI_REPORT_SARIF: "sarif.json"
   NUCLEI_LOG_RESPONSE: "nuclei_resonse.log"


sec_scan_nuclei:      
  stage: verify
  extends: .dast_nuclei
```

## Authors and acknowledgment
- Roland Stumpner
## License
MIT